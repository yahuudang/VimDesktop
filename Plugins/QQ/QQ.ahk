;==============主代码
;此为一个标签，用于主程序识别该代码
QQ:
;定义注释
vim.comment("<回车tx>","回车")
vim.comment("<发送信息tx>","发送信息")
vim.comment("<上一窗口tx>","上一窗口")
vim.comment("<下一窗口tx>","下一窗口")
vim.comment("<上翻tx>","上翻")
vim.comment("<下翻tx>","下翻")
vim.comment("<截图tx>","截图")
vim.comment("<提取聊天信息窗口tx>","提取聊天信息窗口")
vim.comment("<关闭当前窗口tx>","关闭当前窗口")
vim.comment("<帮助tx>","帮助")

vim.mode("normal","TXGuiFoundation")
vim.map("i","<Insert_Mode_TXGuiFoundation>","TXGuiFoundation")

vim.map("{enter}","<回车tx>","TXGuiFoundation")
vim.map("{enter}{enter}","<发送信息tx>","TXGuiFoundation")
vim.map("h","<上一窗口tx>","TXGuiFoundation")
vim.map("l","<下一窗口tx>","TXGuiFoundation")
vim.map("k","<上翻tx>","TXGuiFoundation")
vim.map("j","<下翻tx>","TXGuiFoundation")
vim.map("a","<截图tx>","TXGuiFoundation")
vim.map("z","<提取聊天信息窗口tx>","TXGuiFoundation")
vim.map("x","<关闭当前窗口tx>","TXGuiFoundation")
vim.map("g","<帮助tx>","TXGuiFoundation")

vim.mode("insert","TXGuiFoundation")
vim.map("<c-i>","<Normal_Mode_TXGuiFoundation>","TXGuiFoundation")
return
/*;================检查模式,用于检查焦点Edit1这个控件内激活insert模式,而不用normal模式
;模式检查，此段代码可以直接复制，但请修改ahk_class的值
TXGuiFoundation_CheckMode()
{
    ControlGetFocus,ctrl,AHK_CLASS TXGuiFoundation
    If RegExMatch(ctrl,"Edit1")
        Return 	True
    return  False
}
*/
;============功能代码
<回车tx>:
	send, {Enter}
return
<发送信息tx>:
	send, {Enter 2}
return
<上一窗口tx>:
	send, ^+{Tab}
return
<下一窗口tx>:
	send, ^{Tab}
return
<上翻tx>:
	send, {PgUp}
return
<下翻tx>:
	send, {PgDn}
return
<截图tx>:
	send, +a
return
<提取聊天信息窗口tx>:
	send, !r
return
<关闭当前窗口tx>:
	send, ^w
return
<帮助tx>:
	msgbox, qewsazx
return


;Normal模式
<Normal_Mode_TXGuiFoundation>:
    ;Send,{Esc}
    vim.Mode("normal","TXGuiFoundation")
	;Send,i
return
;insert模式
<Insert_Mode_TXGuiFoundation>:
    vim.Mode("insert","TXGuiFoundation")
	;Send,^i
return